# actev merge-chunks

## Description

Given a list of chunk ids, merges all the chunks system output present in the list.

This command requires the following parameters:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| result-location    | r | True    | path to get the result of the chunks processing                |
| chunk-file         | c | True    | path to generate a chunk file, that summarizes all the processed videos/activities |
| output-file        | o | True    | path to the output file containing merged chunks results                              |
| chunk-ids          | i | False   | list of chunk ids                                              |

## Usage

Generic command:

```
actev merge-chunks -o <path to merging result file> -c <path to summarizing chunk file > -r ~/<chunks directory> -i <list of chunk ids>
```

Example:

```
actev merge-chunks -o ~/output.json -c ~/merging_chunks.json -r ~/chunk_dir/ -i Chunk1 Chunk2 
```