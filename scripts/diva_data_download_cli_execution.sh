#!/bin/bash

# This script demonstrates how to download datasets from the actev-data-repo,
# execute an implemented version of the diva_evaluation_cli, and validate its output.

# It requires a installed version of the diva_evaluation_cli, callable through
# the actev command, python2, python3, virtualenv, and JSON formatted credentials
# to download data from the three following datasets: MEVA, VIRAT-V1, VIRAT-V2.

# Usage:
#  diva_data_download_cli_execution.sh path_to_tmp_directory/ \
# '{"corpus": "MEVA", ... }}' \
# '{"corpus": "VIRAT-V1", ... }}' \
# '{"corpus": "VIRAT-V2", ... }}' \
#  path_to_system_cache_directory/ \
#  path_to_output_directory/

TMP_DIR=$1
ACTEV_DATA_REPO_CREDENTIALS_MEVA=$2
ACTEV_DATA_REPO_CREDENTIALS_VIRAT1=$3
ACTEV_DATA_REPO_CREDENTIALS_VIRAT2=$4
SYSTEM_CACHE_DIR=$5
OUTPUT_DIR=$6

VALIDATION_SET="ActEV-Eval-CLI-Validation-Set3"

if [ -z $TMP_DIR ];then
    echo "Please provide a path to a temporary directory used for virtual environments and to download the videos"
    exit 1
fi
if [ -z "$ACTEV_DATA_REPO_CREDENTIALS_MEVA" ];then
    echo $'Please provide credentials for MEVA, eg: \'{"corpus": "MEVA", "urls": {"https://...: {"type": "xx", "user": "MEVA", "password": "xx"}}}\''
    exit 1
fi
if [ -z "$ACTEV_DATA_REPO_CREDENTIALS_VIRAT1" ];then
    echo $'Please provide credentials for VIRAT-V1, eg: \'{"corpus": "VIRAT-V1", "urls": {"https://...": {"type": "xx", "user": "VIRATv1", "password": "xx"}}}\''
    exit 1
fi
if [ -z "$ACTEV_DATA_REPO_CREDENTIALS_VIRAT2" ];then
    echo $'Please provide credentials for VIRAT-V2, eg: \'{"corpus": "VIRAT-V2", "urls": {"https://...": {"type": "xx", "user": "VIRATv2", "password": "xx"}}}\''
    exit 1
fi
if [ -z "$SYSTEM_CACHE_DIR" ];then
    echo $'Please provide a path to a directory used as a system cache'
    exit 1
fi
if [ -z "$OUTPUT_DIR" ];then
    echo $'Please provide a path to a directory used as a system cache'
    exit 1
fi
set -x

## DATASETS DOWNLOAD
# Clone the data repo
mkdir -p $TMP_DIR && pushd $TMP_DIR
git clone https://gitlab.kitware.com/actev/actev-data-repo $TMP_DIR/actev-data-repo

# Create an environment to run the data download script
pushd $TMP_DIR
virtualenv envpy2 -p python2
popd
source $TMP_DIR/envpy2/bin/activate
python -m pip install jsonschema pandas

# Add the credentials to download the videos
python $TMP_DIR/actev-data-repo/scripts/actev-corpora-maint.py --operation summary --corpus MEVA --add_credential `echo $ACTEV_DATA_REPO_CREDENTIALS_MEVA | tr -d ' '`
python $TMP_DIR/actev-data-repo/scripts/actev-corpora-maint.py --operation summary --corpus VIRAT-V1 --add_credential `echo $ACTEV_DATA_REPO_CREDENTIALS_VIRAT1 | tr -d ' '`
python $TMP_DIR/actev-data-repo/scripts/actev-corpora-maint.py --operation summary --corpus VIRAT-V2 --add_credential `echo $ACTEV_DATA_REPO_CREDENTIALS_VIRAT2 | tr -d ' '`

# Download the video
python $TMP_DIR/actev-data-repo/scripts/actev-corpora-maint.py --operation download --partitions $VALIDATION_SET
deactivate

## CLI EXECUTION
# Prepare the output directory and the output file paths
OUTPUT_DIR=$OUTPUT_DIR/${VALIDATION_SET}_example
mkdir -p $OUTPUT_DIR

FILE_INDEX=$OUTPUT_DIR/${VALIDATION_SET}_file.json
cp $TMP_DIR/actev-data-repo/partitions/$VALIDATION_SET/file-index.json $FILE_INDEX

ACTIVITY_INDEX=$OUTPUT_DIR/${VALIDATION_SET}_activity.json
cp $TMP_DIR/actev-data-repo/partitions/$VALIDATION_SET/activity-index.json $ACTIVITY_INDEX

DESIGNED_CHUNKS=$OUTPUT_DIR/${VALIDATION_SET}_designed_chunks.json
CHUNKS_SUMMARY=$OUTPUT_DIR/${VALIDATION_SET}_chunk.json
OUTPUT_FILE=$OUTPUT_DIR/${VALIDATION_SET}_output.json

# Execute the CLI commands
actev design-chunks -f $FILE_INDEX \
    -a $ACTIVITY_INDEX \
    -o $DESIGNED_CHUNKS

actev exec -f $FILE_INDEX \
    -a $ACTIVITY_INDEX \
    -c $DESIGNED_CHUNKS \
    -v $TMP_DIR/actev-data-repo/corpora \
    -s $SYSTEM_CACHE_DIR \
    -o OUTPUT_FILE \
    -r $CHUNKS_SUMMARY

actev validate-execution -f $FILE_INDEX \
    -a $ACTIVITY_INDEX \
    -o OUTPUT_FILE

