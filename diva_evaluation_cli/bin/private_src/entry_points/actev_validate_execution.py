"""Entry point module: validate-execution

This file should not be modified.
"""
import os

def entry_point(output, reference, activity_index, file_index, result, score):
    """Private entry point.

    Test the execution of the system on each validation data set provided in container_output directory

    Args:
        output (str): Path to experiment output json file
        reference (str): Path to reference json file
        file_index (str): Path to file index json file for test set
        activity_index (str): Path to activity index json file for test set
        result (str): Path to result of the ActEV scorer

    """
    # go into the right directory to execute the script
    current_path = os.path.dirname(__file__)
    execution_validation_dir = os.path.join(current_path, '../implementation/validate_execution')
    installation_script = os.path.join(execution_validation_dir, 'install.sh')
    scoring_cmd = 'python2 ' + os.path.join(execution_validation_dir, 'ActEV_Scorer', 'ActEV_Scorer.py ActEV18_AD -v ')

    scoring_cmd += " -s " + os.path.realpath(output)
    scoring_cmd += " -r " + os.path.realpath(reference) if reference else " "
    scoring_cmd += " -a " + os.path.realpath(activity_index)
    scoring_cmd += " -f " + os.path.realpath(file_index)
    scoring_cmd += " -o " + os.path.realpath(result) if result else " "
    scoring_cmd += " " if score else " -V"

    if not result and score:
        raise Exception("Please provide a -R option when using --score")

    # execute the script
    # status is the exit status code returned by the program
    cmd = 'cd ' + execution_validation_dir + ';' + ' . ' + installation_script + ';' + scoring_cmd
    status = os.system(cmd)
    if status != 0:
        raise Exception("Error occured in install.sh or score.sh")

