"""Entry point module: experiment-init

Implements the entry-point by using Python or any other languages.

"""
import os

def entry_point(file_index, activity_index, chunks,
    video_location, system_cache_dir, config_file=None):
    """Method to complete: you have to raise an exception if an error occured in the program.

    Start servers, starts cluster, etc.

    Args:
        file_index(str): Path to file index json file for test set
        activity_index(str): Path to activity index json file for test set
        chunks (str): Path to chunks json file
        video_location (str): Path to videos content
        system_cache_dir (str): Path to system cache directory
        config_file (str, optional): Path to config file

    """
    raise NotImplementedError("You should implement the entry_point method.")
